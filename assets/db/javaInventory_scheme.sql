CREATE TABLE BoardGame(
id INTEGER PRIMARY KEY,
title TEXT NOT NULL UNIQUE,
price REAL NOT NULL,
players TEXT NOT NULL,
in_stock INTEGER NOT NULL,
minimum_age INTEGER NOT NULL
);

INSERT INTO BoardGame (title, price, players, in_stock, minimum_age) VALUES 
('Draftosaurus', 20, '1-2', 2, 4),
('Raiders on the North Sea', 60, '1-4', 1, 12),
('Jurassic Snack', 22, '1-6', 13, 8);