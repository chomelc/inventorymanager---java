package InventoryTest;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import models.BoardGame;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class BoardGameJUnitTest {
    
    public BoardGameJUnitTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // Test getId()
    @Test
    public void testGetId(){
        BoardGame game = new BoardGame(42, "Title", 42.42, "2-4",42, 4);
        assertEquals(42, game.getId()); 
    } 
    
    // Test getTitle()
    @Test
    public void testGetTitle(){
        BoardGame game = new BoardGame(42, "Title", 42.42, "2-4",42, 4);
        assertEquals("Title", game.getTitle()); 
    }
    
    // Test getPrice()
    @Test
    public void testGetPrice(){
        BoardGame game = new BoardGame(42, "Title", 42.42, "2-4",42, 4);
        assertEquals(42.42, game.getPrice()); 
    }
    
    // Test getPlayers()
    @Test
    public void testGetPlayers(){
        BoardGame game = new BoardGame(42, "Title", 42.42, "2-4",42, 4);
        assertEquals("2-4", game.getPlayers()); 
    }
    
    // Test getInStock()
    @Test
    public void testGetIn_stock(){
        BoardGame game = new BoardGame(42, "Title", 42.42, "2-4",42, 4);
        assertEquals(42, game.getIn_stock()); 
    }
    
    // Test getInStock()
    @Test
    public void testGetMinimum_age(){
        BoardGame game = new BoardGame(42, "Title", 42.42, "2-4",42, 4);
        assertEquals(4, game.getMinimum_age()); 
    }    
    // Test setTitle()
    @Test
    public void testSetTitle(){
        BoardGame game = new BoardGame(42, "Title", 42.42, "2-4",42, 4);
        game.setTitle("New Title");
        assertEquals("New Title", game.getTitle()); 
    }
    
    // Test setPrice()
    @Test
    public void testSetPrice(){
        BoardGame game = new BoardGame();
        game.setPrice(42.42);
        assertEquals(42.42, game.getPrice());
    }
    
    // Test setPlayers()
    @Test
    public void testSetPlayers(){
        BoardGame game = new BoardGame();
        game.setPlayers("2-4");
        assertEquals("2-4", game.getPlayers());
    }
    
    // Test setIn_stock()
    @Test
    public void testSetIn_stock(){
        BoardGame game = new BoardGame();
        game.setIn_stock(42);
        assertEquals(42, game.getIn_stock());
    }
        
    // Test setMinimum_age()
    @Test
    public void testSetMinimum_age(){
        BoardGame game = new BoardGame();
        game.setMinimum_age(4);
        assertEquals(4, game.getMinimum_age());
    }
}
