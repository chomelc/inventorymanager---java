package models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import utils.Database;

public class BoardGame {

    private int id;
    private String title;
    private double price;
    private String players;
    private int in_stock;
    private int minimum_age;

    public BoardGame() {
    }

    // BoardGame constructor
    // constructor of an instance of BoardGame with all of the attributes entered as parameters
    // IN : id, title, price, players, in_stock, minimum_age
    // OUT : /
    public BoardGame(int id, String title, double price, String players, int in_stock, int minimum_age) {
        this.id = id;
        this.title = title;
        this.price = price;
        this.players = players;
        this.in_stock = in_stock;
        this.minimum_age = minimum_age;
    }

    // BoardGame constructor
    // constructor of an instance of BoardGame with all of the attributes entered as parameters except the ID
    // IN : title, price, players, in_stock, minimum_age
    // OUT : /
    public BoardGame(String title, double price, String players, int in_stock, int minimum_age) {
        this.title = title;
        this.price = price;
        this.players = players;
        this.in_stock = in_stock;
        this.minimum_age = minimum_age;
    }

    // getId method
    // a method for getting the ID of an instance
    // IN : /
    // OUT : the ID of the desired instance
    public int getId() {
        return id;
    }

    // setId method
    // a method for setting the ID of an instance
    // IN : the desired ID
    // OUT : /
    public void setId(int id) {
        this.id = id;
    }

    // getTitle method
    // a method for getting the title of an instance
    // IN : /
    // OUT : the title of the desired instance
    public String getTitle() {
        return title;
    }

    // setTitle method
    // a method for setting the title of an instance
    // IN : the desired title
    // OUT : /
    public void setTitle(String title) {
        this.title = title;
    }

    // getPrice method
    // a method for getting the price of an instance
    // IN : /
    // OUT : the price of the desired instance
    public double getPrice() {
        return price;
    }

    // setPrice method
    // a method for setting the price of an instance
    // IN : the desired price
    // OUT : /
    public void setPrice(double price) {
        this.price = price;
    }

    // getPlayers method
    // a method for getting the number of players of an instance
    // IN : /
    // OUT : the number of players of the desired instance
    public String getPlayers() {
        return players;
    }

    // setPlayers method
    // a method for setting the number of players of an instance
    // IN : the desired number of players
    // OUT : /
    public void setPlayers(String players) {
        this.players = players;
    }

    // getIn_stock method
    // a method for getting the stock of an instance
    // IN : /
    // OUT : the stock of the desired instance
    public int getIn_stock() {
        return in_stock;
    }

    // setIn_stock method
    // a method for setting the stock of an instance
    // IN : the desired stock
    // OUT : /
    public void setIn_stock(int in_stock) {
        this.in_stock = in_stock;
    }

    // getMinimum_age method
    // a method for getting the minimum age of an instance
    // IN : /
    // OUT : the minimum age of the desired instance
    public int getMinimum_age() {
        return minimum_age;
    }

    // setMinimum_age method
    // a method for setting the minimum age of an instance
    // IN : the desired minimum age
    // OUT : /
    public void setMinimum_age(int minimum_age) {
        this.minimum_age = minimum_age;
    }

    // overriding toString method
    // a method that returns the content of an object as a string
    // IN : /
    // OUT : the content of the desired object
    @Override
    public String toString(){
        String information="BoardGame{id=%d, title=%s, price=%f, players=%s, in_stock=%s, minimum_age=%d}";
        return String.format(information, this.id, this.title, this.price, this.in_stock, this.players, this.minimum_age);
    }
    
    // all method
    // a method that gets all of the content - the boardgames - of the Database, as a list
    // IN : /
    // OUT : a list of all of the boardgames in the DB
    public static List<BoardGame> all() throws ClassNotFoundException, SQLException {
        Connection connection = Database.getConnection();
        Statement statement = Database.createStatement(connection);
        String query = "SELECT * FROM BoardGame";
        ResultSet resultSet = Database.executeQuery(statement, query);
        List<BoardGame> boardGames = new ArrayList<>();
        while (resultSet.next()) {
            boardGames.add(BoardGame.createFromResultSet(resultSet));
        }
        resultSet.close();
        statement.close();
        connection.close();
        return boardGames;
    }

    //createFromResultSet method
    // a method that returns a BoardGame from a ResultSet
    // IN : the desired ResultSet
    // OUT : a BoardGame
    public static BoardGame createFromResultSet(ResultSet resultSet) throws SQLException {
        return new BoardGame(resultSet.getInt("id"),
                resultSet.getString("title"),
                resultSet.getDouble("price"),
                resultSet.getString("players"),
                resultSet.getInt("in_stock"),
                resultSet.getInt("minimum_age")
        );
    }
    
    // addGameToDB method
    // a method that adds the desired BoardGame to the DB
    // IN : the desired BoardGame
    // OUT : /
    public static void addGameToDB(BoardGame pGame) throws ClassNotFoundException, SQLException {
        try (Connection connection = Database.getConnection()) {
            String query = "INSERT INTO BoardGame (title, price, players, in_stock, minimum_age) VALUES ((?), (?), (?), (?), (?));";
            
            try (PreparedStatement preparedStatement = Database.createPreparedStatement(connection, query)) {
                preparedStatement.setString(1, pGame.getTitle());
                preparedStatement.setDouble(2, pGame.getPrice());
                preparedStatement.setString(3, pGame.getPlayers());
                preparedStatement.setInt(4, pGame.getIn_stock());
                preparedStatement.setInt(5, pGame.getMinimum_age());
                preparedStatement.executeUpdate();
            }
        }
    }
    
    // removeGameFromDB method
    // a method that removes the desired BoardGame from the DB
    // IN : the desired BoardGame
    // OUT : /
    public static void removeGameFromDB(int pId) throws ClassNotFoundException, SQLException {
        try (Connection connection = Database.getConnection()) {
            String query = "DELETE FROM BoardGame WHERE id = (?);";
            
            try (PreparedStatement preparedStatement = Database.createPreparedStatement(connection, query)) {
                preparedStatement.setInt(1, pId);
                preparedStatement.executeUpdate();
            }
        }
    }
    
    // updateGame method
    // a method that updates the desired BoardGame in the DB
    // IN : the ID of the desired BoardGame, the "new" BoardGame that is going to replace the previous one
    // OUT : /
    public static void updateGame(int pId, BoardGame pNewGame) throws SQLException, ClassNotFoundException {
        try (Connection connection = Database.getConnection()) {
            String query = "UPDATE BoardGame SET title = (?), price = (?), players = (?), in_stock = (?), minimum_age = (?)  WHERE id = (?);";
            
            try (PreparedStatement preparedStatement = Database.createPreparedStatement(connection, query)) {
                preparedStatement.setString(1, pNewGame.getTitle());
                preparedStatement.setDouble(2, pNewGame.getPrice());
                preparedStatement.setString(3, pNewGame.getPlayers());
                preparedStatement.setInt(4, pNewGame.getIn_stock());
                preparedStatement.setInt(5, pNewGame.getMinimum_age());
                preparedStatement.setInt(6, pId);
                preparedStatement.executeUpdate();
            }
        }
    }
}
