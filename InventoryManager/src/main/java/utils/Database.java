package utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Database {
    protected static String dbPath;

    // setDbPath method
    // a method that sets the path of the desired DB
    // IN : the path of the desired DB
    // OUT : /
    public static void setDbPath(String path) {
        Database.dbPath = String.format("jdbc:sqlite:%s", path);
    }

    // getDbPath method
    // a method that gets the path of the DB
    // IN : /
    // OUT : the string containing the path of the DB
    public static String getDbPath() {
        return dbPath;
    }
    
    // getConnection method
    // a method for getting the connection with the DB through the DB path
    // IN : /
    // OUT : the connection
    public static Connection getConnection() throws ClassNotFoundException, SQLException {
        Class.forName("org.sqlite.JDBC");
        Connection connection = DriverManager.getConnection(Database.getDbPath());
        return connection;
    }

    // createStatement method
    // a method that creates a statement via the connection with the DB
    // IN : the connection
    // OUT : the statement
    public static Statement createStatement(Connection connection) throws SQLException {
        return connection.createStatement();
    }

    // createPareparedStatement method
    // a method that creates a statement via the connection with the DB, 
    // from a prepared query
    // IN : the connection and the prepared query
    // OUT : the prepared statement
    public static PreparedStatement createPreparedStatement(Connection connection, String query) throws SQLException {
        return connection.prepareStatement(query);
    }

    // executeQuery method
    // a method that executes the desired query in the DB, through to the statement
    // IN : the statement and the desired query
    // OUT : a ResultSet
    public static ResultSet executeQuery(Statement statement, String query) throws SQLException {
        return statement.executeQuery(query);
    }
    
    // executPreparedeQuery method
    // a method that executes the prepared query from the prepared statement
    // IN : the prepared statement
    // OUT : a ResultSet
    public static ResultSet executePreparedQuery(PreparedStatement preparedStatement) throws SQLException{
        return preparedStatement.executeQuery();
    }
}
