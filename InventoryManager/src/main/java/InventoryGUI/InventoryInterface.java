package InventoryGUI;

import com.sun.tools.javac.Main;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import models.BoardGame;
import utils.Database;

public final class InventoryInterface extends javax.swing.JFrame {

    public InventoryInterface() throws ClassNotFoundException, SQLException {
        initComponents();

        // preventing the user to exit by mistake
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent evt) {
                int closeFLag = JOptionPane.showConfirmDialog(null, "Are you sure you want to leave the application?", "Closing the application", JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE);
                if (closeFLag == JOptionPane.YES_OPTION) {
                    evt.getWindow().dispose();
                }
            }
        });

        resetButton.setContentAreaFilled(false);
        stylingInventoryTable(inventoryTable);

        chooseDatabase();
        displayDataInTable();
    }

    // stylingInventoryTable method
    // a method for setting up the basic style of a given JTable
    // IN : the desired JTable
    // OUT : /
    public void stylingInventoryTable(JTable inventoryTable) {
        inventoryTable.getTableHeader().setFont(new Font("Century Gothic", Font.BOLD, 12));
        inventoryTable.getTableHeader().setBackground(new Color(236, 154, 41));
        inventoryTable.getTableHeader().setBorder(null);
        inventoryTable.setBackground(null);
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(SwingConstants.CENTER);
        for (int index = 0; index < 6; index++) {
            inventoryTable.getColumnModel().getColumn(index).setCellRenderer(centerRenderer);
        }
    }

    // chooseDatabase method
    // a method for choosing a DB through a JFileChooser, and setting its path
    // IN : /
    // OUT : /
    public void chooseDatabase() {
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Sqlite3 Databases", "sqlite3", "sql");
        File workingDirectory = new File(System.getProperty("user.dir"));
        databaseChooser.setCurrentDirectory(workingDirectory);
        databaseChooser.setDialogTitle("Choose your database");
        databaseChooser.setFileFilter(filter);
        int returnVal = -1;
        while (returnVal != JFileChooser.APPROVE_OPTION) {
            returnVal = databaseChooser.showOpenDialog(InventoryInterface.this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                Database.setDbPath(databaseChooser.getSelectedFile().getAbsolutePath());
            } else {
                JOptionPane.showMessageDialog(null, "Please select a database.", "No database selected", JOptionPane.INFORMATION_MESSAGE);
            }
        }
    }

    // loadBoardGamesFromDB method
    // a method for loading all board games from the DB,
    // by calling the 'all' method from BoardGames
    // IN : /
    // OUT : a list of all of the board games
    public List<BoardGame> loadBoardGamesFromDB() {
        List<BoardGame> boardGames = new ArrayList<>();

        try {
            boardGames = BoardGame.all();
            // printing content of the DB in the console, for checking
            boardGames.forEach((bg) -> {
                System.out.println(bg);
            });
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        return boardGames;
    }

    // displayDataInTable method
    // a method for displaying the content of the list given by 
    // the loadBoardGamesFromDB method in the JTable
    // IN : /
    // OUT : /
    public final void displayDataInTable() throws ClassNotFoundException, SQLException {
        {
            DefaultTableModel dm = (DefaultTableModel) inventoryTable.getModel();
            while (dm.getRowCount() > 0) {
                dm.removeRow(0);
            }
            DefaultTableModel model;
            model = (DefaultTableModel) inventoryTable.getModel();
            List<BoardGame> games = loadBoardGamesFromDB();
            int i;
            BoardGame _game;
            for (i = 0; i < games.size(); i++) {
                _game = games.get(i);
                model.insertRow(model.getRowCount(), new Object[]{_game.getId(), _game.getTitle(), _game.getIn_stock(), _game.getPlayers(), String.format("%d yo", _game.getMinimum_age()), String.format("%.2f $", _game.getPrice())});
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        databaseChooser = new javax.swing.JFileChooser();
        headerPanel = new javax.swing.JPanel();
        logo = new javax.swing.JLabel();
        subtitleLabel = new javax.swing.JLabel();
        titleLabel = new javax.swing.JLabel();
        changeDBButton = new javax.swing.JButton();
        stockField = new javax.swing.JTextField();
        priceField = new javax.swing.JTextField();
        playersField = new javax.swing.JTextField();
        ageField = new javax.swing.JTextField();
        inventoryScrollPanel = new javax.swing.JScrollPane();
        inventoryTable = new javax.swing.JTable();
        saveButton = new javax.swing.JButton();
        deleteButton = new javax.swing.JButton();
        resetButton = new javax.swing.JButton();
        titleField = new javax.swing.JTextField();
        stockLabel = new javax.swing.JLabel();
        titleFieldLabel = new javax.swing.JLabel();
        playersLabel = new javax.swing.JLabel();
        ageLabel = new javax.swing.JLabel();
        priceLabel = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("BoardGame Passion");
        setBackground(new java.awt.Color(218, 210, 216));
        setResizable(false);
        setSize(new java.awt.Dimension(640, 480));

        headerPanel.setBackground(new java.awt.Color(236, 154, 41));

        logo.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        logo.setForeground(new java.awt.Color(188, 32, 34));
        logo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imgs/board-games_small.png"))); // NOI18N

        subtitleLabel.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        subtitleLabel.setForeground(new java.awt.Color(188, 32, 34));
        subtitleLabel.setText("Manage your inventory");

        titleLabel.setFont(new java.awt.Font("Century Gothic", 1, 24)); // NOI18N
        titleLabel.setForeground(new java.awt.Color(188, 32, 34));
        titleLabel.setText("BoardGame Passion");
        titleLabel.setToolTipText("");

        changeDBButton.setBackground(headerPanel.getBackground());
        changeDBButton.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        changeDBButton.setForeground(new java.awt.Color(188, 32, 34));
        changeDBButton.setText("CHANGE DATABASE");
        changeDBButton.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(188, 32, 34), 3, true));
        changeDBButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                changeDBButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout headerPanelLayout = new javax.swing.GroupLayout(headerPanel);
        headerPanel.setLayout(headerPanelLayout);
        headerPanelLayout.setHorizontalGroup(
            headerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(headerPanelLayout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(logo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(headerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(titleLabel)
                    .addComponent(subtitleLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(changeDBButton, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        headerPanelLayout.setVerticalGroup(
            headerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(headerPanelLayout.createSequentialGroup()
                .addGap(13, 13, 13)
                .addGroup(headerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(logo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(headerPanelLayout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addGroup(headerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(changeDBButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(headerPanelLayout.createSequentialGroup()
                                .addComponent(titleLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(subtitleLabel)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGap(18, 18, 18))
        );

        stockField.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        stockField.setForeground(new java.awt.Color(20, 54, 66));
        stockField.setBorder(null);
        stockField.setSelectedTextColor(new java.awt.Color(20, 54, 66));
        stockField.setSelectionColor(new java.awt.Color(188, 32, 34));

        priceField.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        priceField.setForeground(new java.awt.Color(20, 54, 66));
        priceField.setBorder(null);
        priceField.setSelectedTextColor(new java.awt.Color(20, 54, 66));
        priceField.setSelectionColor(new java.awt.Color(188, 32, 34));

        playersField.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        playersField.setForeground(new java.awt.Color(20, 54, 66));
        playersField.setBorder(null);
        playersField.setSelectedTextColor(new java.awt.Color(20, 54, 66));
        playersField.setSelectionColor(new java.awt.Color(188, 32, 34));

        ageField.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        ageField.setForeground(new java.awt.Color(20, 54, 66));
        ageField.setBorder(null);
        ageField.setSelectedTextColor(new java.awt.Color(20, 54, 66));
        ageField.setSelectionColor(new java.awt.Color(188, 32, 34));

        inventoryScrollPanel.setBorder(null);
        inventoryScrollPanel.setOpaque(false);

        inventoryTable.setBackground(getBackground());
        inventoryTable.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        inventoryTable.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        inventoryTable.setForeground(new java.awt.Color(20, 54, 66));
        inventoryTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "ID", "TITLE", "IN STOCK", "NO. OF PLAYERS", "MIN. AGE", "PRICE"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        inventoryTable.setOpaque(false);
        inventoryTable.setSelectionBackground(new java.awt.Color(188, 32, 34));
        inventoryTable.setSelectionForeground(new java.awt.Color(218, 210, 216));
        inventoryTable.setShowGrid(false);
        inventoryTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                inventoryTableMouseClicked(evt);
            }
        });
        inventoryScrollPanel.setViewportView(inventoryTable);
        if (inventoryTable.getColumnModel().getColumnCount() > 0) {
            inventoryTable.getColumnModel().getColumn(0).setResizable(false);
            inventoryTable.getColumnModel().getColumn(0).setPreferredWidth(5);
            inventoryTable.getColumnModel().getColumn(1).setResizable(false);
            inventoryTable.getColumnModel().getColumn(2).setResizable(false);
            inventoryTable.getColumnModel().getColumn(2).setPreferredWidth(10);
            inventoryTable.getColumnModel().getColumn(3).setResizable(false);
            inventoryTable.getColumnModel().getColumn(3).setPreferredWidth(25);
            inventoryTable.getColumnModel().getColumn(4).setResizable(false);
            inventoryTable.getColumnModel().getColumn(4).setPreferredWidth(10);
            inventoryTable.getColumnModel().getColumn(5).setResizable(false);
            inventoryTable.getColumnModel().getColumn(5).setPreferredWidth(5);
        }

        saveButton.setBackground(new java.awt.Color(236, 154, 41));
        saveButton.setFont(new java.awt.Font("Century Gothic", 1, 11)); // NOI18N
        saveButton.setForeground(new java.awt.Color(20, 54, 66));
        saveButton.setText("ADD / UPDATE");
        saveButton.setBorder(null);
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveButtonActionPerformed(evt);
            }
        });

        deleteButton.setBackground(new java.awt.Color(188, 32, 34));
        deleteButton.setFont(new java.awt.Font("Century Gothic", 1, 11)); // NOI18N
        deleteButton.setForeground(new java.awt.Color(20, 54, 66));
        deleteButton.setText("DELETE");
        deleteButton.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(188, 32, 34), 3));
        deleteButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteButtonActionPerformed(evt);
            }
        });

        resetButton.setBackground(getBackground());
        resetButton.setFont(new java.awt.Font("Century Gothic", 1, 11)); // NOI18N
        resetButton.setForeground(new java.awt.Color(20, 54, 66));
        resetButton.setText("RESET FORM");
        resetButton.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(20, 54, 66), 3, true));
        resetButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resetButtonActionPerformed(evt);
            }
        });

        titleField.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        titleField.setForeground(new java.awt.Color(20, 54, 66));
        titleField.setBorder(null);
        titleField.setSelectedTextColor(new java.awt.Color(20, 54, 66));
        titleField.setSelectionColor(new java.awt.Color(188, 32, 34));

        stockLabel.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        stockLabel.setForeground(new java.awt.Color(20, 54, 66));
        stockLabel.setText("Items in Stock:");

        titleFieldLabel.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        titleFieldLabel.setForeground(new java.awt.Color(20, 54, 66));
        titleFieldLabel.setText("Title:");

        playersLabel.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        playersLabel.setForeground(new java.awt.Color(20, 54, 66));
        playersLabel.setText("Number of Players:");

        ageLabel.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        ageLabel.setForeground(new java.awt.Color(20, 54, 66));
        ageLabel.setText("Minimum Age:");

        priceLabel.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        priceLabel.setForeground(new java.awt.Color(20, 54, 66));
        priceLabel.setText("Price:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(headerPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(inventoryScrollPanel)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(103, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(playersLabel)
                            .addComponent(ageLabel)
                            .addComponent(priceLabel)
                            .addComponent(stockLabel))
                        .addGap(38, 38, 38)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(priceField, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(ageField, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(playersField, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(stockField, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(titleFieldLabel)
                        .addGap(18, 18, 18)
                        .addComponent(titleField, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(81, 81, 81)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(saveButton, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(resetButton, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(deleteButton, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(97, 97, 97))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(headerPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(inventoryScrollPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 104, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(titleField, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(titleFieldLabel, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(stockField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(stockLabel))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(playersField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(playersLabel))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(ageField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(ageLabel)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(saveButton, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(resetButton, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(deleteButton, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(priceField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(priceLabel))
                .addGap(22, 22, 22))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public void resetForm(){
        titleField.setText("");
        stockField.setText("");
        priceField.setText("");
        playersField.setText("");
        ageField.setText("");
    }
    
    // clears the fields by clicking on the 'reset' button
    private void resetButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resetButtonActionPerformed
        resetForm();
    }//GEN-LAST:event_resetButtonActionPerformed

    // adds or updates a game in the DB
    // if a row is selected, the game is updated
    // if no row is selected, the game is added
    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed
        // preventing the user from submitting an empty field
        if (titleField.getText().isEmpty() || playersField.getText().isEmpty()
                || stockField.getText().isEmpty() || priceField.getText().isEmpty()
                || ageField.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "You don't seem to have filled all of the fieldS.", "One or more fields are empty", JOptionPane.INFORMATION_MESSAGE);
        } else {
            String pTitle = titleField.getText();
            String pPlayers = playersField.getText();
            int pIn_stock = Integer.parseInt(stockField.getText());
            double pPrice = Double.parseDouble(priceField.getText());
            int pMinimum_age = Integer.parseInt(ageField.getText());
            BoardGame pBoardGame = new BoardGame(pTitle, pPrice, pPlayers, pIn_stock, pMinimum_age);

            if (inventoryTable.getSelectedRow() == -1) {
                try {
                    BoardGame.addGameToDB(pBoardGame);
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(InventoryInterface.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                try {
                    BoardGame.updateGame((int) inventoryTable.getValueAt(inventoryTable.getSelectedRow(), 0), pBoardGame);
                } catch (SQLException | ClassNotFoundException ex) {
                    Logger.getLogger(InventoryInterface.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            try {
                displayDataInTable();
            } catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(InventoryInterface.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            // reseting the form once the game has been added/updated
            resetForm();
        }
    }//GEN-LAST:event_saveButtonActionPerformed

    // fills the different fields when selecting a specific row
    // numeric values are formated ('$' are removed, and commas are replaced by dots)
    private void inventoryTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_inventoryTableMouseClicked
        int row = inventoryTable.getSelectedRow();
        String readTitle = inventoryTable.getModel().getValueAt(row, 1).toString();
        titleField.setText(readTitle);
        String readInStock = inventoryTable.getModel().getValueAt(row, 2).toString();
        stockField.setText(readInStock);
        String readPlayers = inventoryTable.getModel().getValueAt(row, 3).toString();
        playersField.setText(readPlayers);
        String readMinimumAge = inventoryTable.getModel().getValueAt(row, 4).toString();
        String replaceReadMinimumAge = readMinimumAge.replaceAll("yo","");
        ageField.setText(replaceReadMinimumAge);
        String readPrice = inventoryTable.getModel().getValueAt(row, 5).toString();
        String replaceReadPrice = readPrice.replaceAll("\\$","");
        String finalReplaceReadPrice = replaceReadPrice.replaceAll(",",".");
        priceField.setText(finalReplaceReadPrice);
    }//GEN-LAST:event_inventoryTableMouseClicked

    // deleted a game from the DB
    // if a row is selected, a confirm box appears, and the game is eventually deleted
    // if no row is selected, a message box appears
    private void deleteButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteButtonActionPerformed
        if (inventoryTable.getSelectedRow() == -1) {
            JOptionPane.showMessageDialog(null, "You don't seem to have selected a row to delete.", "No selected row", JOptionPane.INFORMATION_MESSAGE);
        } else {
            try {
                int closeFLag = JOptionPane.showConfirmDialog(null, "Are you sure you want to delete this game?", "Deleting a game", JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE);
                if (closeFLag == JOptionPane.YES_OPTION) {
                    BoardGame.removeGameFromDB((int) inventoryTable.getValueAt(inventoryTable.getSelectedRow(), 0));
                }
                
            } catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(InventoryInterface.class.getName()).log(Level.SEVERE, null, ex);
            }

            try {
                displayDataInTable();
            } catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(InventoryInterface.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            // reseting the form once the game has been deleted
            resetForm();
        }
    }//GEN-LAST:event_deleteButtonActionPerformed

    // enables the user to choose another DB and displaying its content in the JTable
    private void changeDBButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_changeDBButtonActionPerformed
        resetForm();
        chooseDatabase();
        try {
            displayDataInTable();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(InventoryInterface.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_changeDBButtonActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
 /*try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(InventoryInterface.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(InventoryInterface.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(InventoryInterface.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(InventoryInterface.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }*/
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            try {
                new InventoryInterface().setVisible(true);
            } catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(InventoryInterface.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField ageField;
    private javax.swing.JLabel ageLabel;
    private javax.swing.JButton changeDBButton;
    private javax.swing.JFileChooser databaseChooser;
    private javax.swing.JButton deleteButton;
    private javax.swing.JPanel headerPanel;
    private javax.swing.JScrollPane inventoryScrollPanel;
    private javax.swing.JTable inventoryTable;
    private javax.swing.JLabel logo;
    private javax.swing.JTextField playersField;
    private javax.swing.JLabel playersLabel;
    private javax.swing.JTextField priceField;
    private javax.swing.JLabel priceLabel;
    private javax.swing.JButton resetButton;
    private javax.swing.JButton saveButton;
    private javax.swing.JTextField stockField;
    private javax.swing.JLabel stockLabel;
    private javax.swing.JLabel subtitleLabel;
    private javax.swing.JTextField titleField;
    private javax.swing.JLabel titleFieldLabel;
    private javax.swing.JLabel titleLabel;
    // End of variables declaration//GEN-END:variables
}
