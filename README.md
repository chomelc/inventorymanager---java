## Java Project - Inventory Manager

## About

A basic java application for learning :
- [Maven](https://maven.apache.org/)
- Swing
- [JFrame](https://docs.oracle.com/javase/7/docs/api/javax/swing/JFrame.html) & [JPanel](https://docs.oracle.com/javase/7/docs/api/javax/swing/JPanel.html) 
- JDBC with [sqlite](https://bitbucket.org/xerial/sqlite-jdbc//src)
- and some n-tier pattern.

### DataBase | UML Diagramm
![UML diagramm](assets/img/UML.png)

## How to ?
![application](assets/img/app.png)
When the application is launched, the desired database to work with has to be chosen. That database can of course be changed while the application is running, by clicking on the **CHANGE DATABASE** button.

When open, the content of the DB is displayed in the table in the middle of the application. The user can now :
- delete a board game by clicking on the **DELETE** button.
- add a new board game by completing the fields while no row has been selected in the table, and then by clicking on the **SAVE / UPDATE** button.
- update an existing board game by selecting it in the table, then by updating the desired fields, and finally by clicking on the **SAVE / UPDATE** button.
- reset the whole form by clicking on the **RESET FORM** button.

***Note:** Titles have to be unique.*


## Author
> Clémence CHOMEL